﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using CustomEventSystemHandler;

public class ClickEventListenerScript : MonoBehaviour, ICustomMessageTarget
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void Message1() {
        Debug.Log("Received Message1");
    }
}

namespace CustomEventSystemHandler
{
    public interface ICustomMessageTarget : IEventSystemHandler
    {
        void Message1();
    }
}