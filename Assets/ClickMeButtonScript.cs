﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using CustomEventSystemHandler;

public class ClickMeButtonScript : MonoBehaviour
{
    public GameObject eventListener;
    private TextMeshProUGUI textField;
    private TextMeshProUGUI inputField;

    void Start()
    {
        TextMeshProUGUI[] guiComponents = this.transform.parent.parent.GetComponentsInChildren<TextMeshProUGUI>();
        textField = guiComponents[0];
        inputField = guiComponents[2];
    }

    void Update()
    {
        
    }

    public void handleClick() {
        Debug.Log("Clicked button");
        Debug.Log(textField.text);
        textField.text = inputField.text;
        ExecuteEvents.Execute<ICustomMessageTarget>(eventListener, null, (x,y) => x.Message1());
    }
}
